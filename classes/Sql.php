<?php

namespace classes;

/**
 * Class Sql
 * @package classes
 */
class Sql
{
    /** @var Sql $connect */
    private static $connect;

    /** @var \PDO $dbh */
    private static $dbh;

    /** @var string $dsn - например: mysql:host=localhost;dbname=name_db */
    protected static $dsn;

    /** @var string $user - пользователь БД */
    protected static $user;

    /** @var string $password - пароль к БД */
    protected static $password;

    /**
     * Подготавливаем параметры подключения к БД и подключение к БД
     *
     * @inheritdoc
     */
    public function __construct()
    {
        $db_conf = require_once(
            PROJECT_DIR
            . DIRECTORY_SEPARATOR . 'config'
            . DIRECTORY_SEPARATOR . 'db.php'
        );

        self::$dsn = $db_conf['dsn'];
        self::$user = $db_conf['user'];
        self::$password = $db_conf['password'];

        try {
            self::$dbh = new \PDO(self::$dsn, self::$user, self::$password);
            self::$dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        } catch (\PDOException $e) {
            Logger::getLogger()->log($e->getMessage());
        }
    }

    /**
     * Создание единого экземпляра класса
     *
     * @return Sql
     */
    public static function getConnect()
    {
        if (self::$connect === null) {
            self::$connect = new self;
        }

        return self::$connect;
    }

    /**
     * Плиск пользователя по имени
     *
     * @param string      $name
     * @param null|string $surname
     *
     * @return mixed - возвращает масив с данными по пользователю либо FALSE
     *               если запись не найдена
     */
    public function fetch_user($name, $surname = null)
    {
        $sql = 'SELECT * FROM user WHERE name = :name';
        $params = [':name' => $name];

        if ($surname) {
            $sql .= ' AND sur_name = :sur_name';
            $params[':sur_name'] = $surname;
        }

        $stmt = self::$dbh->prepare($sql);
        $stmt->execute($params);

        return $stmt->fetch();
    }

    /**
     * Поиск книг которые выбрал пользователь
     *
     * @param array $user - массив с данными пользователя, полученными из
     *                    метода fetch_user
     *
     * @return array - возвращает масив с данными по выбранным
     *               пользователем книгам либо пустой массив если записи
     *               не найдены
     */
    public function fetch_books(array $user)
    {
        $stmt = self::$dbh->prepare('SELECT * FROM select_book WHERE user_id = :user_id');
        $stmt->execute([
            ':user_id' => $user['id']
        ]);

        return $stmt->fetchAll();
    }

    /**
     * Добавление уникальных выбраных пользователем книг на его
     * полку с очисткой из временной таблицы хранения выбраных
     * книг. Используется транзакция
     *
     * @param array $user  - массив с данными пользователя
     * @param array $books - массив с данными по выбраным пользователем
     *                     книгам
     *
     * @return int - возвращает количество уникальных добавленых книг на
     *             полку пользователя
     */
    public function store_books_to_personal_shelve(array $user, array $books)
    {
        try {
            self::$dbh->beginTransaction();
            $cnt = 0;

            $stmt_shelve = self::$dbh->prepare('INSERT IGNORE INTO shelve (user_id, book_id) VALUES(:user_id, :book_id)');

            foreach ($books as $book) {
                $stmt_shelve->bindValue(':user_id', (int)$user['id'], \PDO::PARAM_INT);
                $stmt_shelve->bindValue(':book_id', (int)$book['book_id'], \PDO::PARAM_INT);
                $stmt_shelve->execute();
                $cnt += $stmt_shelve->rowCount();
            }

            $stmt_book = self::$dbh->prepare('DELETE FROM select_book WHERE user_id = :user_id');
            $stmt_book->bindParam(':user_id', $user['id']);
            $stmt_book->execute();

            self::$dbh->commit();

            return $cnt;
        } catch (\PDOException $e) {
            self::$dbh->rollBack();
            Logger::getLogger()->log($e->getMessage());

            return false;
        }
    }

    /**
     * Закрытие подключения к БД
     *
     * @inheritdoc
     */
    public function __destruct()
    {
        self::$dbh = null;
    }
}