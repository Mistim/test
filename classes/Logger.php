<?php

namespace classes;

/**
 * Class Logger
 * @package classes
 */
class Logger
{
    const MAX_SIZE_FILE = 1048576; // 1MB = 1024 * 1024

    /** @var array $loggers */
    protected static $loggers = [];

    /** @var string $path */
    protected $path = 'runtime' . DIRECTORY_SEPARATOR . 'logs';

    /** @var string $name */
    protected $name;

    /** @var null $file */
    protected $file;

    /** @var resource $fp */
    protected $fp;

    /**
     * Подготовка каталога для хранения логов
     * Открытие файла для записи
     *
     * @param string      $name - используется для названия файла, например: api.log
     * @param null|string $file - или указать путь к нужному файлу
     */
    public function __construct($name, $file = null)
    {
        $this->name = $name;
        $this->file = $file;
        $this->path = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . $this->path;

        if (!file_exists($this->path)) {
            mkdir($this->path, 0644, true);
        }

        $this->open();
    }

    /**
     * Создание единого экземпляра класса для определенного типа лога,
     * например для API
     *
     * @param string      $name - используется для названия файла, например: api.log
     * @param null|string $file - или указать путь к нужному файлу
     *
     * @return Logger
     */
    public static function getLogger($name = 'api', $file = null)
    {
        if (!isset(self::$loggers[$name])) {
            self::$loggers[$name] = new Logger($name, $file);
        }

        return self::$loggers[$name];
    }

    /**
     * Открытие файла для записи
     *
     * @inheritdoc
     */
    public function open()
    {
        $this->copyBackupLog();

        $this->fp = fopen(
            $this->file === null
                ? $this->path . DIRECTORY_SEPARATOR . $this->name . '.log'
                : $this->file,
            'a+'
        );
    }

    /**
     * Подготовка информации для записи лога в файл
     *
     * @param string $message - текстовая информация, которую необходимо записать в лог, например:
     *                        [2016-09-06 23:46:17] Information for log
     */
    public function log($message)
    {
        $log = '[' . date('Y-m-d H:i:s', time()) . '] ';
        $log .= $message;
        $log .= "\n";

        $this->write($log);
    }

    /**
     * Запись лога в файл
     *
     * @param $string - подготовленная текстовая информация, которую необходимо записать в лог
     */
    protected function write($string)
    {
        fwrite($this->fp, $string);
    }

    /**
     * Закрытие файла
     *
     * @inheritdoc
     */
    public function __destruct()
    {
        fclose($this->fp);
    }

    /**
     * Если размер файла лога превышет заданное значение,
     * создаем бекап лога с меткой времени, например: api_1473193964.log
     *
     * @inheritdoc
     */
    protected function copyBackupLog()
    {
        if (file_exists($this->file) && filesize($this->file) >= self::MAX_SIZE_FILE) {
            copy(
                $this->file,
                $this->path . DIRECTORY_SEPARATOR . $this->name . '_' . time() . '.log'
            );

            unlink($this->file);
        }
    }
}