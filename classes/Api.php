<?php

namespace classes;

/**
 * Class Api
 * @package classes
 */
class Api
{
    /** @var  Api $api */
    private static $api;

    /** @var bool $is_need_log - включение/выключение логирования */
    private $is_need_log = true;

    /** @var mixed $result - Последний результат вызова метода run() или цепочки run($command)->run($command) */
    private $result;

    /** @var bool|array $user - результат вызова метода fetch_user() */
    public $user = false;

    /** @var array $books - результат вызова метода fetch_books() */
    public $books = [];

    /** @var int $shelve - результат вызова метода store_books_to_personal_shelve() */
    public $update_shelve;

    /** @var array $access_command - привытные методы, доступные для вызова через run() */
    private $access_command = [
        'fetch_user',
        'fetch_books',
        'store_books_to_personal_shelve',
        'echo_report',
    ];

    /**
     * При попытке вызвать несуществующий метод API
     *
     * @param string $command - метод, который необходимо вызвать
     * @param        ...$args - параметры, которые необходимо передать в вызывемый метод
     *
     * @throws \Exception
     */
    public function __call($command, ...$args)
    {
        if (!method_exists($this, $command)) {
            $message = "You try to call a undefined method: '$command'";
            Logger::getLogger()->log($message);

            throw new \Exception($message);
        }
    }

    /**
     * Выводим отчет, при преобразовании в строку, например: echo new Api()
     *
     * @return string - возвращаем текстовый отчет
     */
    public function __toString()
    {
        return $this->echo_report();
    }

    /**
     * Интерфейс для запуска приватного метода с передачей ему аргументов
     * Также происходит логирование вызова метода с указанием передаваемых аргументов
     * и результата выполнения, например:
     *  [2016-09-06 23:46:17] Run method API::fetch_user
     *        - with args: ["Mari",null]
     *        - result: {"id":"1","0":"1","name":"Mari","1":"Mari","sure_name":"Tester","2":"Tester"}
     *
     * @param string $command - название метода, который должен быть вызван
     * @param mixed  ...$args - передача аргументов вызываемому методу
     *
     * @return Api - возвращается единый
     */
    public function run($command, ...$args)
    {
        if (self::$api === null) {
            self::$api = new self;
        }

        $this->checkAccess($command);
        $this->result = self::$api->$command(...$args);
        $this->setResult($command, $this->result);

        if ($this->is_need_log) {
            Logger::getLogger()->log(
                'Run method API::' . $command . "\n\t"
                . '- with args: ' . json_encode($args) . "\n\t"
                . '- result: ' . json_encode($this->result)
            );
        }

        return $this;
    }

    /**
     * Последний результат вызова метода run()
     * или цепочки run($command)->run($command)
     *
     * @return mixed - массим с информацией выбранной из базы (методы fetch_user и fetch_books)
     *               или TRUE|FALSE (метод store_books_to_personal_shelve)
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Включение/отключение логирования
     *
     * @param bool $status
     *
     * @return $this
     */
    public function enabledLog($status)
    {
        $this->is_need_log = $status;

        return $this;
    }

    /**
     * Поиск пользователя по имени
     *
     * @param string      $name
     * @param null|string $surname
     *
     * @return mixed - возвращает масив с данными по пользователю либо FALSE если запись не найдена
     */
    private function fetch_user($name, $surname = null)
    {
        return Sql::getConnect()->fetch_user($name, $surname);
    }

    /**
     * Поиск книг которые выбрал пользователь
     *
     * @param array $user - массив с данными пользователя, полученными из метода fetch_user
     *
     * @return array - возвращает масив с данными по выбранным пользователем книгам
     * либо пустой массив если записи не найдены
     */
    private function fetch_books($user)
    {
        if (!$user) {
            return [];
        }

        return Sql::getConnect()->fetch_books($user);
    }

    /**
     * Добавление уникальных выбраных пользователем книг на его полку
     * с очисткой из временной таблицы хранения выбраных книг.
     *
     * @param array $user  - массив с данными пользователя
     * @param array $books - массив с данными по выбраным пользователем книгам
     *
     * @return int - возвращает количество уникальных добавленых книг на полку пользователя
     */
    private function store_books_to_personal_shelve($user, array $books)
    {
        if (!$user || count($books) < 1) {
            return false;
        }

        return Sql::getConnect()->store_books_to_personal_shelve($user, $books);
    }

    /**
     * Отчет по результату работы с методами класса
     *
     * @return string
     */
    private function echo_report()
    {
        switch (true) {
            case !$this->user || count($this->user) < 1:
                return 'user not found';

            case count($this->books) < 1:
                return "not selected books for user {$this->user['name']}";

            case $this->update_shelve > 0:
                return "user {$this->user['name']} shelve is updated, number of books: {$this->update_shelve}";

            case $this->update_shelve < 1:
                return "selected books are already on the shelve user {$this->user['name']}";

            default:
                return "user {$this->user['name']} shelve is not updated";
        }
    }

    /**
     * Заполнение публичный свойств класса результатом выполнения соответствующего метода
     *
     * @param string $command - название вызываемого метода
     * @param mixed  $result  - результат выполнения вызываемого метода
     */
    private function setResult($command, $result)
    {
        switch ($command) {
            case 'fetch_user':
                $this->user = $result;
                break;

            case 'fetch_books':
                $this->books = $result;
                break;

            case 'store_books_to_personal_shelve':
                $this->update_shelve = $result;
                break;
        }
    }

    /**
     * Проверка вызываемой метода (комманды)
     *
     * @param $command - название метода (комманды)
     *
     * @throws \Exception
     */
    private function checkAccess($command)
    {
        if (!in_array($command, $this->access_command)) {
            Logger::getLogger()->log("Попытка вызвать запрещенный метод: $command");
            throw new \Exception("Попытка вызвать запрещенный метод: $command");
        }
    }
}