# Test API

## Requirements

* PHP 5.6+
* MySQL 5.5+

# Структура

	classes/    классы приложения
	config/     конфигурационные файлы, например, подключение к базе данных
	data/       файлы данный, например, sql-dump базы данных
	runtime/    файлы, сгенерированные во время выполнения, напримел, логи

# SQL

![alt tag](/data/sql_er_diagram.png "ER Diagram")

Выбранные книги пользователем добавить вручную в таблицу **selected_book**

# Автозагрузка классов

Можно воспользоваться [Composer](http://getcomposer.org/) для генирации автозагрузчика.
Для этого необходимо создать **composer.json** с таким содержимим:

	{
      "require": {
        "php": ">=5.6.0"
      },
      "autoload": {
        "psr-0": {
          "classes": "classes"
        }
      }
    }

Для генирации автозагрузчика запустить из консоли в корне проекта:

	# composer dumpautoload -o

В индексном вайле подключить автозагрузчик:

	require_once __DIR__.'/vendor/autoload.php';

Или воспользоваться [spl_autoload_register()](http://php.net/manual/ru/function.spl-autoload-register.php)

	spl_autoload_register(function ($class) {
        include "$class.php";
    });

# Использование класса Api

Вызов методов (комманд) для обработки данным по пользователю и книгам осуществляется с помощью метода **run()**
Передаваемые аргументы:

- *$command* название метода (комманды), который необхрдимо вызвать
- *...$args* агрументы, которые необходимо передать в вызываемый метод (комманду)

Доступные методы (комманды) вызываемые через run():

- *fetch_user* поиск пользователя по имени
- *fetch_books* книг которые выбрал пользователь
- *store_books_to_personal_shelve* добавление уникальных выбраных пользователем книг на его полку
- *echo_report* отчет по результату работы с методами класса

Публичные методы:

- *run* вызов методов (комманд)
- *getResult* последний результат вызова метода run()
- *enabledLog* включение/отключение логирования

Обычный вызов метода run():

	$result_user = $api->run('fetch_user', $name, $sure_name);

    print_r($result_user->getResult());
    echo PHP_EOL;

    $result_books = $api->run('fetch_books', $api->user);

    print_r($result_books->getResult());
    echo PHP_EOL;

    $result_update_shelve = $api->run('store_books_to_personal_shelve', $api->user, $api->books);

    echo $result_update_shelve;

Вызов метода run() по цепочке:

	$result = $api
        ->run('fetch_user', $name, $sure_name)
        //->enabledLog(false)
        ->run('fetch_books', $api->user)
        ->run('store_books_to_personal_shelve', $api->user, $api->books);

    echo $api . PHP_EOL;