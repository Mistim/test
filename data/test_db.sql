/*
Navicat MySQL Data Transfer

Source Server         : OS
Source Server Version : 50548
Source Host           : localhost:3306
Source Database       : test_db

Target Server Type    : MYSQL
Target Server Version : 50548
File Encoding         : 65001

Date: 2016-09-07 09:37:08
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES ('1', 'Book 1', 'Author 1');
INSERT INTO `book` VALUES ('2', 'Book 2', 'Author 2');
INSERT INTO `book` VALUES ('3', 'Book 3', 'Author 3');
INSERT INTO `book` VALUES ('4', 'Book 4', 'Author 4');
INSERT INTO `book` VALUES ('5', 'Book 5', 'Author 5');
INSERT INTO `book` VALUES ('6', 'Book 6', 'Author 6');
INSERT INTO `book` VALUES ('7', 'Book 7', 'Author 7');
INSERT INTO `book` VALUES ('8', 'Book 8', 'Author 8');
INSERT INTO `book` VALUES ('9', 'Book 9', 'Author 9');
INSERT INTO `book` VALUES ('10', 'Book 10', 'Author 10');

-- ----------------------------
-- Table structure for select_book
-- ----------------------------
DROP TABLE IF EXISTS `select_book`;
CREATE TABLE `select_book` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `book_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_select_book_to_user_id` (`user_id`),
  KEY `fk_select_book_to_book_id` (`book_id`),
  CONSTRAINT `fk_select_book_to_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_select_book_to_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of select_book
-- ----------------------------

-- ----------------------------
-- Table structure for shelve
-- ----------------------------
DROP TABLE IF EXISTS `shelve`;
CREATE TABLE `shelve` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `book_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ind_unique_use_book` (`user_id`,`book_id`),
  KEY `fk_shelve_to_user_id` (`user_id`),
  KEY `fk_shelve_to_book_id` (`book_id`) USING BTREE,
  CONSTRAINT `fk_shelve_to_book_id` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_shelve_to_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of shelve
-- ----------------------------

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `sure_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ind_unique_user_name` (`name`,`sure_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('4', 'Andrew', 'Tester');
INSERT INTO `user` VALUES ('1', 'Mari', 'Tester');
INSERT INTO `user` VALUES ('2', 'Nick', 'Tester');
INSERT INTO `user` VALUES ('5', 'Olga', 'Tester');
INSERT INTO `user` VALUES ('3', 'Vasiliy', 'Tester');
