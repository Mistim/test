<?php

defined('PROJECT_DIR') or define('PROJECT_DIR', dirname(__FILE__));

// Использование Composer для генерации автозагрузчика
// Запустить: composer dumpautoload -o
//require_once __DIR__.'/vendor/autoload.php';

// Использование spl_autoload_register
spl_autoload_register(function ($class) {
    include "$class.php";
});

$name = isset($_GET['UserName']) ? $_GET['UserName'] : null;
$sure_name = isset($_GET['SurName']) ? $_GET['SurName'] : null;

$api = new \classes\Api();

// Вызов run() по цепочке
$result = $api
    ->run('fetch_user', $name, $sure_name)
    //->enabledLog(false)
    ->run('fetch_books', $api->user)
    ->run('store_books_to_personal_shelve', $api->user, $api->books);

echo $api . PHP_EOL;

// Обычный вызов run()
/*$result_user = $api->run('fetch_user', $name, $sure_name);

print_r($result_user->getResult());
echo PHP_EOL;

$result_books = $api->run('fetch_books', $api->user);

print_r($result_books->getResult());
echo PHP_EOL;

$result_update_shelve = $api->run('store_books_to_personal_shelve', $api->user, $api->books);

echo $result_update_shelve;*/